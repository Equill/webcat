# Webcat

It's a knowledge-management system (yes, another one!) built on [Restagraph](https://equill.github.io/restagraph/). By "basic," I mean that it's Restagraph with a schema designed for this purpose.

For a _very_ basic web-GUI, see [cl-webcat](https://github.com/equill/cl-webcat), but remember I warned you that it's basic.


## License

`Webcat` is licensed under GPLv3.
