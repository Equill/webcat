VERSION='0.12.0'
cp ../../schemas/webcat.json ./
docker build . -t equill/webcat:$VERSION
rm webcat.json
